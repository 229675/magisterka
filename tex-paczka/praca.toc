\babel@toc {polish}{}
\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {polish}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Wst\IeC {\k e}p}{1}{chapter.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Wprowadzenie}{3}{chapter.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Epidemiologia i etiologia zespo\IeC {\l }u stopy cukrzycowej}{3}{section.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Diagnostyka stopy cukrzycowej}{6}{section.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Przegl\IeC {\k a}d literatury dotycz\IeC {\k a}cej segmentacji owrzodzenia w stopie cukrzycowej}{7}{chapter.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Konwolucyjne sieci neuronowe}{9}{chapter.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Wst\IeC {\k e}p do konwolucyjnych sieci neuronowych}{9}{section.4.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Om\IeC {\'o}wienie dotychczasowych architektur CNN maj\IeC {\k a}cych zastosowanie w zagadnieniu diagnostyki stopy cukrzycowej}{10}{section.4.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}GoogLeNet 2014}{10}{subsection.4.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}DFUNet 2018}{11}{subsection.4.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}DFU\_QUTNet 2019}{12}{subsection.4.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.4}Wnioski}{14}{subsection.4.2.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliografia}{17}{chapter*.12}% 
