import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers


class DFUNet:
    image_size = (224, 224, 3)
    model = None

    def __init__(self):
        inputs = keras.Input(shape=image_size, name='img')
        conv1 = layers.Conv2D(
            64, 7, strides=(2, 2),
            padding='same', activation='relu')(inputs)
        max_pool1 = layers.MaxPool2D(
            (3, 3), padding='same',  strides=(
                2, 2))(conv1)
        norm1 = tf.nn.lrn(max_pool1)
        conv2 = layers.Conv2D(
            64, 1, strides=(1, 1),
            padding='same', activation='relu')(norm1)
        conv3 = layers.Conv2D(
            192, 3, strides=(1, 1),
            padding='same', activation='relu')(conv2)
        max_pool2 = layers.MaxPool2D(
            (3, 3), padding='same',  strides=(
                2, 2))(conv3)

        parallel1_conv1 = layers.Conv2D(
            32, 1, strides=(1, 1),
            padding='same',
            activation='relu')(max_pool2)
        parallel1_conv2 = layers.Conv2D(
            64, 3, strides=(1, 1), padding='same',
            activation='relu')(max_pool2)
        parallel1_conv3 = layers.Conv2D(
            128, 5, strides=(1, 1), padding='same',
            activation='relu')(max_pool2)
        parallel1_output = layers.concatenate([
            parallel1_conv1, parallel1_conv2, parallel1_conv3])
        norm2 = tf.nn.lrn(parallel1_output)

        max_pool3 = layers.MaxPool2D(
            (3, 3), padding='same',  strides=(
                2, 2))(norm2)
        parallel2_conv1 = layers.Conv2D(
            32, 1, strides=(1, 1), padding='same',
            activation='relu')(max_pool3)
        parallel2_conv2 = layers.Conv2D(
            64, 3, strides=(1, 1), padding='same',
            activation='relu')(max_pool3)
        parallel2_conv3 = layers.Conv2D(
            128, 5, strides=(1, 1), padding='same',
            activation='relu')(max_pool3)
        parallel2_output = layers.concatenate([
            parallel2_conv1, parallel2_conv2, parallel2_conv3])
        norm3 = tf.nn.lrn(parallel2_output)

        parallel3_conv1 = layers.Conv2D(
            32, 1, strides=(1, 1), padding='same',
            activation='relu')(norm3)
        parallel3_conv2 = layers.Conv2D(
            64, 3, strides=(1, 1), padding='same',
            activation='relu')(norm3)
        parallel3_conv3 = layers.Conv2D(
            128, 5, strides=(1, 1), padding='same',
            activation='relu')(norm3)
        parallel3_output = layers.concatenate([
            parallel3_conv1, parallel3_conv2, parallel3_conv3])
        norm4 = tf.nn.lrn(parallel3_output)

        max_pool4 = layers.MaxPool2D(
            (3, 3), padding='same',  strides=(
                2, 2))(norm4)
        parallel4_conv1 = layers.Conv2D(
            32, 1, strides=(1, 1), padding='same',
            activation='relu')(max_pool4)
        parallel4_conv2 = layers.Conv2D(
            64, 3, strides=(1, 1), padding='same',
            activation='relu')(max_pool4)
        parallel4_conv3 = layers.Conv2D(
            128, 5, strides=(1, 1), padding='same',
            activation='relu')(max_pool4)
        parallel4_output = layers.concatenate([
            parallel4_conv1, parallel4_conv2, parallel4_conv3])
        norm5 = tf.nn.lrn(parallel4_output)

        max_pool5 = layers.MaxPool2D(
            (7, 7), padding='valid', strides=(
                1, 1))(norm5)
        dense1 = layers.Dense(1000)(max_pool5)
        outputs = layers.Dense(2)(dense1)

        model = keras.Model(inputs, outputs, name="DFUNet")

    def summary(self):
        model.summary()
        keras.utils.plot_model(model, 'dfunet.png', show_shapes=True)
