import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
from glob import glob
import seaborn as sns
from skimage.io import imread

# ścieżka
base_skin_dir = os.path.join('../..', 'data/mnist')
imageid_path_dict = {os.path.splitext(os.path.basename(x))[0]: x
                     for x in glob(os.path.join(base_skin_dir, '*', '*.jpg'))}

# typy zmian
lesion_type_dict = {
    'nv': 'Melanocytic nevi',
    'mel': 'dermatofibroma',
    'bkl': 'Benign keratosis-like lesions ',
    'bcc': 'Basal cell carcinoma',
    'akiec': 'Actinic keratoses',
    'vasc': 'Vascular lesions',
    'df': 'Dermatofibroma'
}

# czytaj meta
tile_df = pd.read_csv(os.path.join(base_skin_dir, 'HAM10000_metadata.csv'))
tile_df['path'] = tile_df['image_id'].map(imageid_path_dict.get)
tile_df['cell_type'] = tile_df['dx'].map(lesion_type_dict.get)
tile_df['cell_type_idx'] = pd.Categorical(tile_df['cell_type']).codes


print(tile_df.shape)

print(tile_df.sample(3))
print(tile_df.describe(exclude=[np.number]))
print(np.unique(tile_df['cell_type']))

# czytaj obrazy
tile_df['image'] = tile_df['path'].map(imread)
tile_df['image'].map(lambda x: x.shape).value_counts()


n_samples = 3
fig, m_axs = plt.subplots(6, n_samples, figsize=(4*n_samples, 3*6))
for n_axs, (type_name, type_rows) in zip(
        m_axs, tile_df.sort_values(['cell_type']).groupby('cell_type')):
    n_axs[0].set_title(type_name)
    print(type_name)
    for c_ax, (_, c_row) in zip(n_axs, type_rows.sample(
            n_samples, random_state=18).iterrows()):
        c_ax.imshow(c_row['image'])
        c_ax.axis('off')
fig.savefig('category_samples.png', dpi=300)

rgb_info_df = tile_df.apply(
    lambda x: pd.Series(
        {'{}_mean'.format(k): v for k,
         v in zip(['Red', 'Green', 'Blue'],
                  np.mean(x['image'],
                          (0, 1)))}),
    1)
gray_col_vec = rgb_info_df.apply(lambda x: np.mean(x), 1)
for c_col in rgb_info_df.columns:
    rgb_info_df[c_col] = rgb_info_df[c_col]/gray_col_vec
rgb_info_df['Gray_mean'] = gray_col_vec
print(rgb_info_df.sample(3))

for c_col in rgb_info_df.columns:
    tile_df[c_col] = rgb_info_df[c_col].values  # we cant afford a copy


sns.pairplot(
    tile_df
    [['Red_mean', 'Green_mean', 'Blue_mean', 'Gray_mean', 'cell_type']],
    hue='cell_type', plot_kws={'alpha': 0.5})
