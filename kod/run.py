import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import tensorflow.keras.models as models
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow import keras
from tensorflow.keras.callbacks import ReduceLROnPlateau
import datetime
from DFUNet import DFUNet
from DFU_QUTNet import DFU_QUTNet
from moja_3_1_2_3_100_Drop import Moja_3_1_2_3_100_Drop

# rozmiar obrazów
img_width, img_height = 224, 224

# zbiory treningowe
train_data_dir = '../data/ISIC/train'
validation_data_dir = '../data/ISIC/val'
test_data_dir = '../data/ISIC/test'

# ilość próbek obrazów dla smaples_per_epoch
nb_train_samples = 100
nb_validation_samples = 10
epochs = 50
batch_size = 10

# generacja danych treingowych
train_datagen = ImageDataGenerator(
        rescale=1./255,            # normalizacja wartości 0x0..0xFF do 0..1
        shear_range=0.2,
        zoom_range=0.2,
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True)

val_datagen = ImageDataGenerator(rescale=1./255)
test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
    train_data_dir,
    target_size=(img_height, img_width),
    batch_size=batch_size,
    class_mode='binary')

validation_generator = train_datagen.flow_from_directory(
    validation_data_dir,
    target_size=(img_height, img_width),
    batch_size=batch_size,
    class_mode='binary')

test_generator = train_datagen.flow_from_directory(
    test_data_dir, target_size=(img_height, img_width),
    batch_size=batch_size, class_mode='binary')


# budowa modelu

moja = Moja_3_1_2_3_100_Drop()
# dfunet = DFUNet(2)
dfuqutnet = DFU_QUTNet()

# dd = keras.Sequential([
#     keras.layers.Flatten(input_shape=(224, 224, 3)),
#     keras.layers.Dense(128, activation='relu'),
#     keras.layers.Dense(10)
# ])

models = [moja.model]

for model in models:
    learning_rate_reduction = ReduceLROnPlateau(monitor='val_accuracy',
                                                patience=3,
                                                verbose=1,
                                                factor=0.5,
                                                min_lr=0.00001)
    log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(
        log_dir=log_dir, histogram_freq=1)

    model.compile(optimizer='adam',
                  loss=tf.keras.losses.BinaryCrossentropy(from_logits=False),
                  metrics=['accuracy'])

    history = model.fit(
                train_generator,
                steps_per_epoch=nb_train_samples // batch_size,
                epochs=epochs,
                validation_data=validation_generator,
                validation_steps=nb_validation_samples // batch_size,
                    callbacks=[learning_rate_reduction, tensorboard_callback])
#    print(history.history.keys())

    # generacja wykresów
#     plt.figure()
#     plt.plot(history.history['accuracy'], 'orange', label='Training accuracy')
#     plt.plot(history.history['val_accuracy'],
#              'blue', label='Validation accuracy')
#     plt.plot(history.history['loss'], 'red', label='Training loss')
#     plt.plot(history.history['val_loss'], 'green', label='Validation loss')
#     plt.legend()
#     plt.show()
#
    loss, accuracy = model.evaluate(test_generator, verbose=1, batch_size=20)

    print("Test: accuracy = %f  ;  loss = %f" % (accuracy, loss))

    # zapisz model
    # model.save('model.h5')
