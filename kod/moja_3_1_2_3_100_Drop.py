from tensorflow import keras
from tensorflow.keras import layers


class Moja_3_1_2_3_100_Drop:
    image_size = (224, 224, 3)
    model = None

    def __init__(self):
        inputs = keras.Input(shape=self.image_size, name='img')
        conv1 = layers.Conv2D(64, 3, strides=2, padding='same')(inputs)
        conv2 = layers.Conv2D(64, 1, strides=2, padding='same')(conv1)
        norm1 = layers.BatchNormalization()(conv2)
        relu1 = layers.ReLU()(norm1)
        pool0 = layers.AvgPool2D(pool_size=7, strides=2)(relu1)

        parallel1_conv1 = layers.Conv2D(
            32, 1, strides=1, padding='same', activation="relu")(pool0)
        parallel1_conv2 = layers.Conv2D(
            32, 1, strides=1, padding='same', activation="relu")(pool0)
        parallel1_conv3 = layers.Conv2D(
            32, 1, strides=1, padding='same', activation="relu")(pool0)
        parallel1_conv4 = layers.Conv2D(
            64, 3, strides=1, padding='same', activation='relu')(
            parallel1_conv2)
        parallel1_conv5 = layers.Conv2D(
            128, 5, strides=1, padding='same', activation='relu')(
            parallel1_conv3)
        parallel1_output = layers.concatenate(
            [parallel1_conv1, parallel1_conv4, parallel1_conv5])
        norm2 = layers.BatchNormalization()(parallel1_output)
        relu2 = layers.ReLU()(norm2)
        pool1 = layers.AvgPool2D(pool_size=7, strides=1)(relu2)

        parallel2_conv1 = layers.Conv2D(
            32, 1, strides=1, padding='same', activation="relu")(pool1)
        parallel2_conv2 = layers.Conv2D(
            32, 1, strides=1, padding='same', activation="relu")(pool1)
        parallel2_conv3 = layers.Conv2D(
            32, 1, strides=1, padding='same', activation="relu")(pool1)
        parallel2_conv4 = layers.Conv2D(
            64, 3, strides=1, padding='same', activation='relu')(
            parallel2_conv2)
        parallel2_conv5 = layers.Conv2D(
            128, 5, strides=1, padding='same', activation='relu')(
            parallel2_conv3)
        parallel2_output = layers.concatenate(
            [parallel2_conv1, parallel2_conv4, parallel2_conv5])
        norm3 = layers.BatchNormalization()(parallel2_output)
        relu3 = layers.ReLU()(norm3)
        pool2 = layers.AvgPool2D(pool_size=7, strides=1)(relu3)

        parallel3_conv1 = layers.Conv2D(
            32, 1, strides=1, padding='same', activation="relu")(pool2)
        parallel3_conv2 = layers.Conv2D(
            32, 1, strides=1, padding='same', activation="relu")(pool2)
        parallel3_conv3 = layers.Conv2D(
            32, 1, strides=1, padding='same', activation="relu")(pool2)
        parallel3_conv4 = layers.Conv2D(
            64, 3, strides=1, padding='same', activation='relu')(
            parallel3_conv2)
        parallel3_conv5 = layers.Conv2D(
            128, 5, strides=1, padding='same', activation='relu')(
            parallel3_conv3)
        parallel3_output = layers.concatenate(
            [parallel3_conv1, parallel3_conv4, parallel3_conv5])
        norm4 = layers.BatchNormalization()(parallel3_output)
        relu4 = layers.ReLU()(norm4)
        pool3 = layers.AvgPool2D(pool_size=7, strides=1)(relu4)

        avg_pool = layers.GlobalAvgPool2D()(pool3)
        dense1 = layers.Dense(100)(avg_pool)
        drop = layers.Dropout(rate=0.5)(dense1)
        dense2 = layers.Dense(2)(drop)
        outputs = layers.Softmax()(dense2)

        self.model = keras.Model(inputs, outputs, name="moja1")
#         optimizer = keras.optimizers.Adam(learning_rate=0.001)
#         self.model.compile(optimizer)

    def summary(self):
        self.model.summary()
        keras.utils.plot_model(self.model, 'moja1.png', show_shapes=True)
