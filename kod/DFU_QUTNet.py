from tensorflow import keras
from tensorflow.keras import layers


class DFU_QUTNet:
    image_size = (224, 224, 3)
    model = None

    def __init__(self):
        inputs = keras.Input(shape=self.image_size, name='img')
        conv1 = layers.Conv2D(32, 3, strides=1,
                              padding='same')(inputs)
        norm1 = layers.BatchNormalisation()(conv1)
        relu1 = layers.ReLU()(norm1)

        parallel1_conv1 = layers.Conv2D(
            32, 3, strides=2, padding='same')(relu1)
        parallel1_norm1 = layers.BatchNormalisation()(parallel1_conv1)
        parallel1_relu1 = layers.ReLU()(parallel1_norm1)
        parallel1_conv2 = keras.Conv2D(
            32, 3, strides=1, padding='same')(parallel1_relu1)
        parallel1_norm2 = keras.BatchNormalisation()(parallel1_conv2)

        parallel1_conv3 = layers.Conv2D(
            32, 3, strides=2, padding='same')(norm1)
        parallel1_norm3 = layers.BatchNormalisation()(parallel1_conv3)
        parallel1_relu2 = layers.ReLU()(parallel1_norm3)
        parallel1_conv4 = layers.Conv2D(
            32, 3, strides=1, padding='same')(parallel1_relu2)
        parallel1_norm4 = layers.BatchNormalisation()(parallel1_conv4)

        parallel1_output = layers.Add(parallel1_norm4, parallel1_norm2)
        relu2 = layers.ReLU()(parallel1_output)

        parallel2_conv1 = layers.Conv2D(
            64, 3, strides=2, padding='same')(relu2)
        parallel2_norm1 = layers.BatchNormalisation()(parallel2_conv1)
        parallel2_relu1 = layers.ReLU()(parallel2_norm1)
        parallel2_conv2 = keras.Conv2D(
            64, 3, strides=1, padding='same')(parallel2_relu1)
        parallel2_norm2 = keras.BatchNormalisation()(parallel2_conv2)

        parallel2_conv3 = layers.Conv2D(
            64, 3, strides=2, padding='same')(relu2)
        parallel2_norm3 = layers.BatchNormalisation()(parallel2_conv3)
        parallel2_relu2 = layers.ReLU()(parallel2_norm3)
        parallel2_conv4 = layers.Conv2D(
            64, 3, strides=1, padding='same')(parallel2_relu2)
        parallel2_norm4 = layers.BatchNormalisation()(parallel2_conv4)

        parallel2_output = layers.Add(parallel2_norm4, parallel2_norm2)
        relu3 = layers.ReLU()(parallel2_output)

        parallel3_conv1 = layers.Conv2D(
            128, 3, strides=2, padding='same')(relu3)
        parallel3_norm1 = layers.BatchNormalisation()(parallel3_conv1)
        parallel3_relu1 = layers.ReLU()(parallel3_norm1)
        parallel3_conv2 = keras.Conv2D(
            128, 3, strides=1, padding='same')(parallel3_relu1)
        parallel3_norm2 = keras.BatchNormalisation()(parallel3_conv2)

        parallel3_conv3 = layers.Conv2D(
            128, 3, strides=2, padding='same')(relu3)
        parallel3_norm3 = layers.BatchNormalisation()(parallel3_conv3)
        parallel3_relu2 = layers.ReLU()(parallel3_norm3)
        parallel3_conv4 = layers.Conv2D(
            128, 3, strides=1, padding='same')(parallel3_relu2)
        parallel3_norm4 = layers.BatchNormalisation()(parallel3_conv4)

        parallel3_output = layers.Add(parallel3_norm4, parallel3_norm2)
        relu4 = layers.ReLU()(parallel3_output)

        parallel4_conv1 = layers.Conv2D(
            256, 3, strides=2, padding='same')(relu4)
        parallel4_norm1 = layers.BatchNormalisation()(parallel4_conv1)
        parallel4_relu1 = layers.ReLU()(parallel4_norm1)
        parallel4_conv2 = keras.Conv2D(
            256, 3, strides=1, padding='same')(parallel4_relu1)
        parallel4_norm2 = keras.BatchNormalisation()(parallel4_conv2)

        parallel4_conv3 = layers.Conv2D(
            256, 3, strides=2, padding='same')(relu4)
        parallel4_norm3 = layers.BatchNormalisation()(parallel4_conv3)
        parallel4_relu2 = layers.ReLU()(parallel4_norm3)
        parallel4_conv4 = layers.Conv2D(
            256, 3, strides=1, padding='same')(parallel4_relu2)
        parallel4_norm4 = layers.BatchNormalisation()(parallel4_conv4)

        parallel4_output = layers.Add(parallel4_norm4, parallel4_norm2)
        relu5 = layers.ReLU()(parallel4_output)

        av_pool1 = layers.AveragePooling2D(
            8, strides=1, padding='valid')(relu5)
        dense1 = layers.Dense(100)(av_pool1)
        drop1 = layers.Dropout(rate=0.5)(dense1)
        dense2 = layers.Dense(2)(drop1)
        outputs = layers.Softmax()(dense2)

        self.model = keras.Model(inputs, outputs, name="DFU_QUTNet")

    def summary(self):
        self.model.summary()
        keras.utils.plot_model(self.model, 'dfu_qutnet.png', show_shapes=True)
