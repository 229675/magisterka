\chapter{Konwolucyjne sieci neuronowe}
\thispagestyle{chapterBeginStyle}

\section{Wstęp do konwolucyjnych sieci neuronowych}
Splotowe sieci neuronowe (\emph{convolutional neural networks}, CNN) stanowią jedno z najczęściej wykorzystywanych narzędzi w praktyce \emph{Deep Learning}. Jest to rodzaj sieci neuronowych, w których zastąpiono w co najmniej jednej warstwie mnożenie macierzy danych z macierzami wag operacją splotu \cite{goodfellow_sieci_2018}. Zastosowanie operacji splotu pozwala na znaczne uproszczenie przetwarzanych informacji poprzez ekstrakcję z danych cech wyższego rzędu \cite{patterson_deep_2018}. Operację splotu można rozumieć jako filtrowanie wprowadzanego sygnału o topologii regularnej siatki w domenie przestrzeni, jak w wypadku obrazów, lub czasu, na przykład przy nagraniu mowy \cite{goodfellow_sieci_2018}.

Historia sieci CNN sięga lat pięćdziesiątych, kiedy David Hubel i Torsten Wiesel badali funkcjonowanie kory wzrokowej ssaków\cite{goodfellow_sieci_2018}. W późniejszych latach ich odkrycia posłużyły jako inspiracja dla algorytmów uczenia maszynowego. Warstwy sieci konwolucyjnej maja na celu odwzorowanie cech właściwości pierwszorzędowej kory wzrokowej (V1). Podobnie jak w V1, neurony sieci konwolucyjnej są zorganizowane przestrzennie w sposób analogiczny do obrazu – \enquote{górne} neurony sieci odzworowują górne elementy obrazu, przekazując wyekstrahowaną cechę do obrazu stanowiącego odwzorowanie. Komórki proste kory wzrokowej są odwzorowane w sztucznych sieciach neuronowych jako jednostki wykrywania, które w małym, zlokalizowanym polu recepcyjnym dokonują linioweo przetworzenia fragmentu obrazu. Inspiracją dla jednostek redukcji (\emph{pooling} w sieciach neuronowych są natomiast komórki złożone, które odpowiadają na cechy podobnie jak komórki proste, lecz są niewrażliwe na małe przesunięcia położenia cechy czy zmiany w oświetleniu \cite{goodfellow_sieci_2018}.

Jak już wspomniano, podstawowym pojęciem wykorzystywanym w technologii splotowych sieci neuronowych jest splot. W wypadku zagadnienienia dwuwymiarowego, jak w przypadku obrazów, operacja ta jest definiowana następująco:
\[S(i, j) = (K * I)(i, j) = \displaystyle\sum_m \displaystyle\sum_n I(i - m, j - n)K(m, n) \]
gdzie \(I\) to obraz, \(K\) to kernel (jądro) – zbiór paramerów dostosowywanych przez system uczący się, a \(S\) to odwzorowanie cech (\emph{feature map}) obrazu \(I\). W odróżnieniu od klasycznych sieci neuronowych z warstwami w pełni połączonymi, gdzie macierz parametrów opisuje między każdą jednostką wejściową i wyjściową, co przy obrazach składających się z milionów pikseli wiązałoby się z ogromnym kosztem obliczeniowym, w sieciach konwolucyjnych kernel jest znacznie mniejszy od obrazu, pozwalając jednocześnie na odfiltrowanie istotnych szczegółów obrazu, jak na przykład krawędzi przedmiotów. W każdej kolejnej warstwie konwolucyjnej wykrywane są powiązania charakterystyczne dla co raz większych pól recepcyjnych oryginalnego obrazu, prowadząc w końcu do powstania zbioru cech stanowiących \enquote{esencję} obrazu o rozmiarach wystarczająco małych, by mogły być zwektoryzowane i sklasyfikowane przez bardziej tradycyjny algorytm uczenia maszynowego, na przykład sieci w pełni połączoną lub maszynę wektorów nośnych (\emph{support vector machine}, SVM) \cite{goodfellow_sieci_2018, patterson_deep_2018}. Warto zauważyć, że splot nie jest operacją ekwiwalentną w stosunku do obrazów poddanych zmianie skali czy rotacji, w związku z czym konieczne jest odpowiednie wzbogacenie zbioru uczącego o odpowiednio zmodyfikowane kopie obrazów \cite{goodfellow_sieci_2018}.
\begin{figure}[ht!]
 \begin{center}
  \includegraphics[width=0.5\textwidth]{Typical_cnn.png}
 \end{center}
\caption{{\color{dgray}Klasyczna architektura sieci konwolucyjnej. Według Aphex34 (Wikimedia) / CC BY-SA, url: https://commons.wikimedia.org/wiki/File:Typical\_cnn.png}} \label{cnn}
\end{figure}
W architekturze sieci konwolucyjnych (rys. \ref{cnn}) można wyróżnić warstwę wejściową, mającą za zadanie załadowanie obrazu – w wypadku obrazu RGB dane przyjmują formę tensora o rozmiarach odpowiadających wysokości i szerokości obrazu w pikselach i jego liczbie kanałów. Następnie dane są przekazywane do warstw rozpoznających cechy, do których na ogół należą warstwy konwolucyjne, wykonujące filtrację obrazu, warstwy rektyfikujące (ReLU) pozwalające na przyspieszenie treningu poprzez eliminację wartości mniejszych od 0, a także warstwy łączące (zwane również redukcyjnymi, ang. \emph{pooling}), których zadaniem jest podpróbkowanie – zmniejszenie obrazu – w celu redukcji obciążenia procesora \cite{goodfellow_sieci_2018, patterson_deep_2018, geron_splotowe_2018, ramsundar_splotowe_2020}. Przeważnie warstwy łączące zwracają wartość maksymalną lub średnią z obszaru małego kernela \cite{geron_splotowe_2018, ramsundar_splotowe_2020}. Ostatnim etapem obróbki obrazu w CNN są warstwy w pełni połączone, pozwalające na wykonanie założonego zadania, takiego jak klasyfikacja, lokalizacja przedmiotów czy segmentacja zagadnienia \cite{patterson_deep_2018, ramsundar_splotowe_2020}.

\section{Omówienie dotychczasowych architektur CNN mających zastosowanie w zagadnieniu diagnostyki stopy cukrzycowej}
W niniejszej sekcji zostaną omówione architektury konwolucyjnych sieci neuronowych: odpowiadająca aktualnemu stanowi technologii GoogLeNet, a także zaprojektowanych specjalnie do diagnostyki stopy cukrzycowej architektur opisanych w pracach Goyala et al. \cite{goyal_dfunet_2018} oraz Alzubaidiego et al. \cite{alzubaidi_dfu_qutnet_2019}. Zostaną wskazane innowacje zawarte w tych modelach oraz motywacje stojące za nimi. Na koniec zostaną wyciągnięte wnioski dotyczące kierunku rozwoju architektury proponowanej tej pracy.

Zaprezentowane architektury (wraz z innymi) zostały przetestowane na jednakowym zbiorze danych w \cite{alzubaidi_dfu_qutnet_2019}, co pozwala na jednoznaczne określenie ich dopasowania do zagadnienia. Wynik porównania przedstawiono w tabeli \ref{porownanie}. Pod względem parameru F1, stanowiącego średnią harmoniczną precyzji i czułości, najlepsza jest DFU\_QUTNet, potem DFUNet i pretrenowany na ogólnych obrazach i następnie trenowany ponownie obrazami stopy cukrzycowej GoogLeNet.
\begin{table}[ht!]
 \begin{center}
  \includegraphics[width=0.75\textwidth]{porownanie.png}
 \end{center}
\caption{{\color{dgray}Ewaluacja kilku architektur CNN w zagadnieniu stopy cukrzycowej. Według \cite{alzubaidi_dfu_qutnet_2019}}} \label{porownanie}
\end{table}

\subsection{GoogLeNet 2014}
Sieć GoogLeNet została zaprojektowana przez Christiana Szegedy'ego wraz z zespołem Google Research \cite{szegedy_going_2015, geron_splotowe_2018}. Sieć zwyciężyła w konkursie ILSVRC w 2014 polegającym na przypisywaniu ogólnych obrazów do wielu klas \cite{geron_splotowe_2018}. Jej istotnymi cechami jest znacznie oszczędniejsze od konkurencji wykorzystanie parametrów, co poprawia wydajność obliczeniową, a także zwiększona głębokość, co poprawia wydajność ekstrakcji cech. Dokonano tego poprzez wprowadzenie koncepcji \emph{modułów incepcyjnych} (rys. \ref{modul-incepcyjny}), które stanowią miniaturową sieć konwolucyjną traktowaną jak pojedyńcza warstwa całej sieci. Warstwy incepcyjne dzięki swojej wewnętrznej strukturze pozwalają na wykrywanie bardziej złożonych wzorców przy jednoczesnym zachowaniu wydajności obliczeniowej.
\begin{figure}[ht!]
 \begin{center}
  \includegraphics[width=0.5\textwidth]{modul-incepcyjny.png}
 \end{center}
\caption{{\color{dgray}Moduł incepcyjny GoogLeNet. Według \cite{szegedy_going_2015}}} \label{modul-incepcyjny}
\end{figure}
Moduł incepcyjny rozdziela sygnał wejściowy na 4 równoległe ścieżki, w których wykorzystuje się jądra o różnych rozmiarach, co pozwala na detekcję cech o różnych skalach jednocześnie \cite{geron_splotowe_2018}. Ciekawym rozwiązaniem jest zastosowanie warstw ograniczających (\emph{bottleneck layers}) o kernelu 1×1, które nie dokonują zmian w obrazie, a jedynie redukują wymiarowość, co jest szczególnie przydatne przed kosztownymi obliczeniowo warstwami 3×3 i 5×5 \cite{geron_splotowe_2018}. Następnie wyniki poszczególnych ścieżek są poddawane konkantenacji (połączeniu) i przekazywane do następnej warstwy.
\begin{table}[ht!]
 \begin{center}
  \includegraphics[width=0.75\textwidth]{tabela-googlenet.png}
 \end{center}
\caption{{\color{dgray}Architektura GoogLeNet. Według \cite{szegedy_going_2015}.}} \label{googlenet}
\end{table}

Całość architektury GoogLeNet jest zaprezentowana w tabeli \ref{googlenet}. Model składa się z 22 warstw, z czego 9 stanowi moduły incepcyjne. W procesie pracy sieci obraz jest wielokrotnie zmniejszany i normalizowany celem zwiększenia wydajności obliczeniowej. Obecność warstwy łączącej uśredniającej (\emph{avg pool}) na końcu procesu ma na celu wymuszenie tworzenia map cech pozwalających na efektywną klasyfikcję, co pozwala na zmniejszenie ilości warstw w pełni połącznych. To podejście pozwala na zmniejszenie ilości paramerów i ryzyka przetrenowania w porównaniu do wcześniejszych architektur, np. AlexNet \cite{geron_splotowe_2018, szegedy_going_2015}.


\subsection{DFUNet 2018}
Charakterystyczną cechą architektury DFUNet jest jej trójdzielność: składa się z części wejściowej, inspirowanej GoogLeNet, części zawierającej przetwarzanie przez równległe warstwy konwolucyjne i ostatecznie warstw klasyfikujących (rys. \ref{dfunet}). Warstwy równoległe wykonują jednoczesne filtrowanie kernelami o rozmiarach 1×1, 3×3 i 5×5, co podobnie jak w przypadku GoogLeNet ma na celu wykrywanie cech o różnych rozmiarach \cite{goyal_dfunet_2018}. Nie występują jednak warstwy ograniczające. Ogólna struktura DFUNet jest przedstawiona na tabeli \ref{dfunet-tabela}.
\begin{figure}[ht!]
 \begin{center}
  \includegraphics[width=0.5\textwidth]{dfunet.png}
 \end{center}
\caption{{\color{dgray}Ogólny widok architektury DFUNet. Według \cite{goyal_dfunet_2018}.}} \label{dfunet}
\end{figure}
 
Zaprezentowana sieć ma 22 warstwy i jest znacząco uproszczona w stosunku do GoogLeNet, co można powiązać z faktem, że jest zaprojektowana do dużo prostszego zastosowania klasyfikacji binarnej. Podobnie jak w GoogLeNet, wyniki działania warstw równoległych są konkantenowane. Celem dodania warstw poolingujących po warstwach równoległych jest redukcja wymiarowości \cite{goyal_dfunet_2018}. 
\begin{table}[ht!]
 \begin{center}
  \includegraphics[width=0.75\textwidth]{dfunet-tabela.png}
 \end{center}
\caption{{\color{dgray}Architektura DFUNet. Według \cite{goyal_dfunet_2018}.}} \label{dfunet-tabela}
\end{table}
\subsection{DFU\_QUTNet 2019}
Ostatnia z prezentowanych architektur (tabela \ref{dfu_qutnet_tabela}, rys. \ref{dfu_qutnet}), DFU\_QUTNet, również wykorzystuje przetwarzanie równoległe obrazu. Jednakże, występują pewne różnice względem GoogLeNet i inspirowanej nią DFUNet. W tym wypadku w każdym rozgałęzieniu algorytmu wykonywane są dwukrotnie konwolucje z kernelem 3×3, razem z rektyfikacją i normalizacją. Rozgałęzienia są natomiast sumowane zamiast konkantenacji \cite{alzubaidi_dfu_qutnet_2019}. Zastosowana jest warstwa łącząca uśredniająca (\emph{global average pooling}), by zminimalizować stratę cech typową dla maksymalnego łączenia (\emph{max pooling}). Warstwa \emph{dropout} wprowadza element losowości do modelu, by uniknąć efektu \emph{overfittingu} i poprawić wyniki \cite{alzubaidi_dfu_qutnet_2019}. Na koniec wynik obróbki przez warstwy konwolucyjne jest klasyfikowany przez klasyfikator SoftMax, SVM lub KNN, przy czym to SVM uzyskało najlepsze wyniki (tabela \ref{porownanie}) \cite{alzubaidi_dfu_qutnet_2019}.
\begin{figure}[ht!]
 \begin{center}
    \rotatebox[origin=c]{-90}{\includegraphics[width=0.5\textwidth]{dfu_qutnet.png}}
 \end{center}
\caption{{\color{dgray}Ogólny widok architektury DFU\_QUTNet. Według \cite{alzubaidi_dfu_qutnet_2019}.}} \label{dfu_qutnet}
\end{figure}

\begin{table}[ht!]
 \begin{center}
    \includegraphics[width=0.65\textwidth]{dfu_qutnet_tabela.png}
 \end{center}
\caption{{\color{dgray}Architektura DFU\_QUTNet. Według \cite{alzubaidi_dfu_qutnet_2019}}} \label{dfu_qutnet_tabela}
\end{table}

DFU\_QUTNet posiada łącznie 58 warstw. Niestety, w pracy \cite{alzubaidi_dfu_qutnet_2019} określono zużycia zasobów obliczeniowych tak głębokiego modelu. O zaletach tego rozwiązania świadczą jednak wysokie wyniki (tabela \ref{porownanie}), według badaczy lepsze od wyników współczenej praktyki medycznej.

\subsection{Wnioski}
Analiza dotychczasowych rozwiązań wskazuje na ogólne tendencje w dziedzinie, wyłaniając kilka koncepcji, które można będzie wykorzystać przy projektowaniu proponowanego modelu. Należą do nich równoległe warstwy konwolucyjne, warstwy ograniczające, \emph{global average pooling} czy warstwy \emph{dropout}. 
